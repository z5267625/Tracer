To get the bluetooth code to run on a raspberry Pi, you need to have a Raspberry pi 3B+ at minimum.

Once you have got the OS running you will need to install the following. Make sure the OS has been updated.

#bluetooth

sudo apt-get install bluetooth

#You may need to install some bluetooth libs

sudo apt-get install libbluetooth-dev

#PyBluez

sudo python3 -m pip install pybluez


# once you have the libs and packages as required, you need to change some details in some files.

navigate to:

	 /etc/systemd/system/bluetooth.target.wants

an modify the file, bluetooth.service using

sudo nano bluetooth.service

in this file append the line ExcStart=/usr/lib/bluetooth/bluetoothd

	TO

	ExcStart=/usr/lib/bluetooth/bluetoothd --compat --noplugin=sap -E

# once this file is moderfied go into the bash and type:

sudo systemctl daemon-reload
sudo systemctl restart bluetooth

sudo sdptool add SP

Once this is done the bluetooth should work..........should
 