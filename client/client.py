
import os
import pygubu
import requests
import uuid
import sqlite3
import pickle
import json
import datetime
from flask import jsonify
import bluetooth
import subprocess
import _thread
import time


PROJECT_PATH = os.path.dirname(__file__)
PROJECT_UI = os.path.join(PROJECT_PATH, "main_window.ui")
DATABASE="contacts.db"
SAVEFILE="objs.pkl"
UPLOAD_API_ENDPOINT="https://tracer.ultroc.com/api/contacts/add"
AUTH_API_ENDPOINT = "https://tracer.ultroc.com/api/users/login"
ADD_API_ENDPOINT="https://tracer.ultroc.com/api/users/add"


class MainWindowApp:

    userID=None
    key=None
    name=None
    email=None
    phone=None
    running=False

    def __init__(self):
        self.builder = builder = pygubu.Builder()
        builder.add_resource_path(PROJECT_PATH)
        builder.add_from_file(PROJECT_UI)
        self.mainwindow = builder.get_object('frame1')
        builder.connect_callbacks(self)

        #Check if there is a savefile
        if os.path.exists(SAVEFILE):
            #Restore variables saved from last use
            with open(SAVEFILE,'rb') as f:  
                self.userID, self.key, self.name,self.email,self.phone = pickle.load(f)

            #Set buttons and text according to a successful registration
            self.builder.get_object('entry_name').configure(state="disabled")
            self.builder.get_object('entry_email').configure(state="disabled")
            self.builder.get_object('entry_phone').configure(state="disabled")
            self.name=self.builder.get_variable('txt_name').set(self.name)
            self.email=self.builder.get_variable('txt_email').set(self.email)
            self.phone=self.builder.get_variable('txt_phone').set(self.phone)
            self.builder.get_variable('txt_registration').set("Success")
            self.builder.get_object('button_register').configure(state="disabled")

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    #A function to authenticate to an API endpoint and return a token
    def do_auth(self,username, password, url) -> dict:
        
        data = {
            "userID": username,
            "key": password
        }

        # sending post request and saving response as response object
        r = requests.post(url, json=data)
        # extracting response text
        response_text = r.text
        token = json.loads(response_text)

        return token
    
    # A wrapper to post to an authenticated endpoint once a token is obtained
    def do_post(self,url,data,access_token: str):
        
        headers = {
            'Authorization': ('{}'.format(access_token['token']))
        }
        response = requests.post(url,json=data,headers=headers)

        return response
    
    
    def add_contact(self, contact):
        
        #here is the code to add a contact into the database when it occurs
        #We add both ours and theirs incase our UUID changes
        
        #The contacts' UUID received
        dtg = datetime.datetime.utcnow().strftime("%B %d, %Y %I:%M%p")

        conn = sqlite3.connect(DATABASE)
        sql = ''' INSERT INTO CONTACTS(ID_FROM,ID_TO,DTG)
            VALUES(?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql,(self.userID,contact,dtg))
        all_contacts = cur.execute('SELECT * FROM CONTACTS;').fetchall()
        conn.commit()

        conn.close()
        # Update the display of how many contacts are in the local database
        self.builder.get_variable('txt_contactcount').set(len(all_contacts))


        #This bit is for debugging purposes
        #This will show the state of the local database after each contact is added
        print("\n")
        print(all_contacts)
        print("\n")
        

    def register(self, event=None):
        #When user registers two UUIDs are used, one private and one public
        #Public UUID is exchanged during a 'contact'
        #Priavate UUID is used to verify that the device is authorized to add to the server database (logging in)
        #Both keys saved to device
        self.userID=str(uuid.uuid4())
        self.key=str(uuid.uuid4())
        self.name=self.builder.get_variable('txt_name').get()
        self.email=self.builder.get_variable('txt_email').get()
        self.phone=self.builder.get_variable('txt_phone').get()
        
       
        #Send JSON POST to add user to the user database


        data = {
            "userID":self.userID,
            "key":self.key,
            "name": self.name,
            "email": self.email,
            "phone": self.phone,
            
        }
        
        resp = requests.post(ADD_API_ENDPOINT, json=data)
                
        #If we get a successful response from the server
        if (resp.status_code == 200):
            #Change status label
            self.builder.get_variable('txt_registration').set("Registered Successfully")
            
            #Disable text entry
            self.builder.get_object('entry_name').configure(state="disabled")
            self.builder.get_object('entry_email').configure(state="disabled")
            self.builder.get_object('entry_phone').configure(state="disabled")

            #Save all our details for next load
            with open(SAVEFILE, 'wb') as f:
                pickle.dump([self.userID, self.key, self.name,self.email,self.phone], f)
            
        else:
            self.builder.get_variable('txt_registration').set("Failed")
        
    def reset(self, event=None):
        #Reset our registration details
        self.builder.get_object('entry_name').configure(state="normal")
        self.builder.get_object('button_register').configure(state="normal")
        self.builder.get_object('entry_email').configure(state="normal")
        self.builder.get_object('entry_phone').configure(state="normal")
        self.builder.get_variable('txt_registration').set("Not Registered")

        if os.path.exists(SAVEFILE):
            os.remove(SAVEFILE)
    
    #def broadcast(self,clientuuid):
        #broadcast code
          
    def bluetooth(self):
        
        self.running=True
        deviceList =[] # device list that will store nearby device UUID and timestamp
        temp = [] # list to temporarily store UUID to compare to current devices
        clientuuid = self.userID

        #-------------------- turn the bluetooth on--------------------------
        
        cmd = 'sudo hciconfig hci0 piscan'
        subprocess.check_output(cmd, shell =True)

        # ----------------------advertise the COVID DETAILS----------------------  

        server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        server_sock.bind(("", bluetooth.PORT_ANY))
        server_sock.listen(1)
        print("Broadcasting COVIDTRACER service...")
        print("-----------------------------------") # remove after testing
              
        bluetooth.advertise_service(sock = server_sock, name="COVIDTRACER",service_id = clientuuid)
  
        #------------------------ find other services and record ----------------------------------
        while self.running:  
            target = "all"
            if target == "all":
                target = None
            
            # Bluetooth sequentially scans CH 37, CH 38, CH 39 for advertisements over some set scan interval.
            # If scanning is not synchronised to the current channel that is advertising, adverts will be missed.
            # Scan is run three times and offset each time by 1/3 the scan interval to align with each channel at some point.
            services = []
            print("Searching for other users...")
            # Scan 3 times
            for i in range(3):
                start = time.time()
                scan = bluetooth.find_service(address=target)
                stop = time.time()
                interval = stop - start # This calculates the scan interval each loop
                #print("Scan interval: ", interval)
                for svc in scan:
                    if svc not in services:
                        services.append(svc)
                time.sleep(interval/3) # Sleep for a third of the scan interval before scanning again
                       
            for svc in services: 
                if svc["name"] == 'COVIDTRACER':
                    temp.append(svc["service-id"]) # add all devices to the temp list
                    if len(deviceList) is 0: # if device list empty just append new device
                        deviceList.append({"uuid": svc["service-id"], "time": time.time()})
                    else: # else check to see if it already exists so time isn't overwritten
                        for dev in range(len(deviceList)):
                            if svc["service-id"] not in deviceList[dev].values():
                                deviceList.append({"uuid": svc["service-id"], "time": time.time()})
            print(f"{len(deviceList)} user(s) found!")
            
            for dev in range(len(deviceList)): # loop to compare device list to most recently found
                if deviceList[dev]["uuid"] not in temp: # if device isn't seen anymore, remove it
                    contact = deviceList[dev].copy() # copy data to contact var
                    deviceList.pop(dev) # remove from device list
                    if (time.time() - contact["time"]) >= 60.0: # Time in seconds (15 minutes = 900s)
                        contactstring = str(contact["uuid"])
                        contactlower = contactstring.lower()
                        self.add_contact(contact=contactlower) # add contact if it was proximal for required time
                        print("Added contact: ", contact["uuid"])
            
            temp = [] # reset the temp list
            
            print(deviceList) # remove after testing
        
        #-------------------- turn the bluetooth off--------------------------  
        
        print("-----------------------------------")
        print('COVIDTRACER Broadcast Stopped')# remove this line after testing
        cmd_off = 'sudo hciconfig hci0 noscan' # turn off bluetooth scan
        subprocess.check_output(cmd_off, shell =True)

        # If scan manually disabled, add any contacts left in deviceList that meet time reqs
        print("Adding valid contacts...")
        for dev in deviceList:
            if (time.time() - dev["time"]) >= 60.0:
                contactstring = str(dev["uuid"])
                contactlower = contactstring.lower()
                self.add_contact(contactlower) # add contact if it was proximal for required time
                print("Added contact: ", dev["uuid"])
    
    def start(self, event=None):
        self.builder.get_variable('txt_status').set("Running")
        self.builder.get_variable('txt_uploadstatus').set("")
        _thread.start_new_thread(self.bluetooth,())


    def stop(self, event=None):
        self.builder.get_variable('txt_status').set("Stopped")
        self.running=False


    def upload(self, event=None):
        #Upload entries in the client.db to the server using the api endpoint
        #Need to incorporate authentication (login/token/request), see test_post.py

        #This is a test addition, this UUID is already a contact on the server database init
        
        conn = sqlite3.connect(DATABASE)
        conn.row_factory = self.dict_factory
        cur = conn.cursor()
        all_contacts = cur.execute('SELECT * FROM contacts;').fetchall()
        conn.commit()
        conn.close()

        #Get an auth token from the server
        token=self.do_auth(self.userID,self.key,AUTH_API_ENDPOINT)

        for contact in all_contacts:
            data = {
                "ID_FROM": contact['ID_FROM'],
                "ID_TO": contact['ID_TO'],
                "DTG":contact['DTG'],
            }
                   
            resp = self.do_post(UPLOAD_API_ENDPOINT,data,token)
                               
            if (resp.status_code == 200):
                self.builder.get_variable('txt_uploadstatus').set("Upload Successfull")

                conn = sqlite3.connect(DATABASE)
                # Delete the entry of this one contact after it has been uploaded
                sql = ''' DELETE FROM CONTACTS WHERE ID_FROM=? AND ID_TO=? AND DTG=? '''
                conn.row_factory = self.dict_factory
                cur = conn.cursor()
                cur.execute(sql,(contact['ID_FROM'],contact['ID_TO'],contact['DTG']))
                all_contacts = cur.execute('SELECT * FROM CONTACTS;').fetchall()
                conn.commit()
                conn.close()
                # Update the display of how many contacts are in the local database
                self.builder.get_variable('txt_contactcount').set(len(all_contacts))

                # Debugging - see contents of database
                print("\n")
                print(all_contacts)
                print("\n")

            else:
                self.builder.get_variable('txt_uploadstatus').set("Upload Failed")
            

    def run(self):
        self.mainwindow.mainloop()

if __name__ == '__main__':
    import tkinter as tk
    #root = tk.Tk()
    app = MainWindowApp()
    app.run()
