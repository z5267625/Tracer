import sqlite3
import os
import csv

DATBASE_NAME="contacts.db"

if os.path.exists(DATBASE_NAME):
    os.remove(DATBASE_NAME)
else:
    print("Can not delete the file as it doesn't exists")

conn = sqlite3.connect(DATBASE_NAME) 
c = conn.cursor()

# Create table - USERS
# Stores user registrations for the contact tracing database
c.execute('''CREATE TABLE CONTACTS
             ([generated_id] INTEGER PRIMARY KEY,[ID_FROM] text,[ID_TO] text, [DTG] text)''')
              
conn.commit()

#CONFIRM DATABASE
conn = sqlite3.connect(DATBASE_NAME)
c = conn.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")
print(c.fetchall())

c = conn.execute('select * from CONTACTS')
names = list(map(lambda x: x[0], c.description))
print(names)


print("Database initialised")