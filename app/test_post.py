from os import access, pathconf_names
import requests
import uuid
import json
from flask import jsonify

#Python script to test the authorization functionality of the app
#This needs to be incorporated into the client.py UI

userID=uuid.uuid4().hex
key=uuid.uuid4().hex
name="TestUser"
email="test@user.com"
phone="999"

AUTH_API_ENDPOINT = "http://127.0.0.1:5000/api/users/login"
REGISTRATION_API_ENDPOINT = "http://127.0.0.1:5000/api/users/add"
CONTACTS_API_ENDPOINT = "http://127.0.0.1:5000/api/contacts/all"

def do_registration():
    data = {
        "userID" : userID,
        "key" : key,
        "name" : name,
        "email" : email,
        "phone" : phone
    }

    r = requests.post(url=REGISTRATION_API_ENDPOINT, json=data)
    
    return r
    


def do_auth(username, password, url=AUTH_API_ENDPOINT) -> dict:
    data = {
        "userID": username,
        "key": password
    }

    # sending post request and saving response as response object
    r = requests.post(url=url, json=data)

    # extracting response text
    response_text = r.text

    token = json.loads(response_text)

    return token



def do_get(url, access_token: str):
    print(access_token)
    headers = {
        'Authorization': ('{}'.format(access_token['token']))
    }
    
    print(headers)
    response = requests.get(url, headers=headers)

    return response

#With authorisation
r = do_registration()
print(r)
token = do_auth(userID, key)
response = do_get(CONTACTS_API_ENDPOINT,token)
print(response.text)

'''
#without authorization
response = requests.get(CONTACTS_API_ENDPOINT)
print(response.text)
'''
