import sqlite3
import os
import csv

DATABASE_NAME="tracer.db"

def insert_dummy_users():

    conn = sqlite3.connect(DATABASE_NAME)
    c = conn.cursor()
    
    with open('./MOCK_USERS.csv','r') as fin: 
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin) # comma is default delimiter
        to_db = [(i['UserID'], i['KEY'], i['NAME'], i['EMAIL'], i['PHONE']) for i in dr]

    c.executemany("INSERT INTO USERS (UserID, KEY, NAME, EMAIL, PHONE) VALUES (?, ?, ?, ?, ?);", to_db)
    conn.commit()
    conn.close()

def insert_dummy_contacts():
    
    conn = sqlite3.connect(DATABASE_NAME)
    c = conn.cursor()
    
    with open('./MOCK_CONTACTS.csv','r') as fin: 
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin) # comma is default delimiter
        to_db = [(i['ID_FROM'], i['ID_TO'], i['DTG']) for i in dr]

    c.executemany("INSERT INTO CONTACTS (ID_FROM, ID_TO, DTG) VALUES (?, ?, ?);", to_db)
    conn.commit()
    conn.close()

def insert_admin_user():
    conn = sqlite3.connect(DATABASE_NAME)
    sql = ''' INSERT INTO USERS(userID,NAME,KEY,EMAIL,PHONE,ROLE)
              VALUES(?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql,("admin","admin","password","admin@admin.com","9999","admin"))
    conn.commit()



if os.path.exists(DATABASE_NAME):
    os.remove(DATABASE_NAME)
else:
    print("Can not delete the file as it doesn't exists")

conn = sqlite3.connect(DATABASE_NAME) 
c = conn.cursor()

# Create table - USERS
# Stores user registrations for the contact tracing database
# Any user added withour a role specified is a "user", the other option is "admin"
c.execute('''CREATE TABLE USERS
             ([generated_id] INTEGER PRIMARY KEY,[UserID] text, [KEY] text,[NAME] text, [EMAIL] text, [PHONE] text,[ROLE] text DEFAULT 'user')''')
          
# Create table - CONTACTS
# Stores UUIDs of 'contacts' to cross reference with USERS database and track people
c.execute('''CREATE TABLE CONTACTS
             ([generated_id] INTEGER PRIMARY KEY,[ID_FROM] text,[ID_TO] text, [DTG] text, FOREIGN KEY (ID_FROM) REFERENCES USERS (UserID) ON DELETE RESTRICT, FOREIGN KEY (ID_TO) REFERENCES USERS (UserID) ON DELETE RESTRICT)''')
   
             
conn.commit()

#CONFIRM DATABASE
conn = sqlite3.connect(DATABASE_NAME)
c = conn.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")
print(c.fetchall())

c = conn.execute('select * from USERS')
names = list(map(lambda x: x[0], c.description))
print(names)

c = conn.execute('select * from CONTACTS')
names = list(map(lambda x: x[0], c.description))
print(names)

#insert some dummy data if desired for testing
insert_dummy_users()
#insert_dummy_contacts()
insert_admin_user()

print("Database initialised")



