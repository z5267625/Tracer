from flask import Flask,render_template, jsonify, request, make_response, redirect, session, url_for
import sqlite3
import os
import datetime
import logging
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from functools import wraps

app = Flask(__name__)

DATABASE_URL="tracer.db"

#All logs to file in /app directory
logger = logging.getLogger('werkzeug')
handler = logging.FileHandler('access.log')
logger.addHandler(handler)

#Some database setup for user authentication
app.config['SECRET_KEY']='Th1s1ss3cr3t'
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///'+DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

#This class represents the USERS table of the database
class Users(db.Model):
    generated_id = db.Column(db.Integer, primary_key=True)
    userID = db.Column(db.String)
    key = db.Column(db.String)
    name = db.Column(db.String)
    email = db.Column(db.String)
    phone = db.Column(db.String)
    role = db.Column(db.String)

#Used for validating API endpoints
#Call this via decoration (@token_validation(<required role>)) under any route that requires authorisation (or user attributes)
#Ensure to add "current_user" as an input parameter to the subsequent function
def token_validation(role):
    def decorator(f):
        @wraps(f)
        def wrapper(*args,**kwargs):

            if 'Authorization' in request.headers:
                token = request.headers['Authorization']
            else:
                return jsonify({'message': 'a valid token is missing'})

            try:
                data = jwt.decode(token, app.config['SECRET_KEY'],algorithms=["HS256"])
                current_user = Users.query.filter_by(userID=data['userID']).first()
                if(current_user.role==role):
                    return f(current_user, *args, **kwargs)
                else:
                    return jsonify({'message': 'token has incorrect role'})
            except:
                return jsonify({'message': 'token is invalid'})

        return wrapper
    return decorator

#Used for validating HTML routes
#Call this via decoration (@login_validation(<required role>)) under any route that requires authorisation (or user attributes)
#Ensure to add "current_user" as an input parameter to the subsequent function
def login_validation(role):
    def decorator(f):
        @wraps(f)
        def wrapper(*args,**kwargs):

            try:  
                #user=None if there is no user session     
                user=session.get('userID')
                current_user = Users.query.filter_by(userID=user).first()

                if(current_user.role==role):
                    return f(current_user, *args, **kwargs)
                else:
                    return render_template('login.html')
            except:
                return render_template('login.html')
        
        return wrapper
    return decorator


#This function interprets sql rows so they make sense
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

#Display all database users
def users_all():
    conn = sqlite3.connect(DATABASE_URL)
    conn.row_factory = dict_factory
    cur = conn.cursor()
    all_users = cur.execute('SELECT * FROM USERS;').fetchall()
    conn.commit()
    conn.close()
    return all_users

def contacts_all():
    conn = sqlite3.connect(DATABASE_URL)
    conn.row_factory = dict_factory
    cur = conn.cursor()
    #This query matches the two UserIDs in the contacts and matches them to users in the user database
    
    all_contacts = cur.execute('SELECT a.NAME AS NAME_CONTACT, a.EMAIL AS EMAIL_CONTACT,a.PHONE AS PHONE_CONTACT,\
        b.NAME AS NAME_ORIGINATOR, b.EMAIL AS EMAIL_ORIGINATOR, b.PHONE AS PHONE_ORIGINATOR, c.DTG AS DTG FROM CONTACTS c\
        INNER JOIN USERS a ON c.ID_TO=a.UserID INNER JOIN USERS b ON c.ID_FROM=b.UserID ;').fetchall()
    conn.commit()
    conn.close()
    return all_contacts

"""
Webapp Routes section
"""

@app.route('/',methods = ['POST'])
def login_post():
    
    userID = request.form.get('userID')
    password = request.form.get('password')
    
    user = Users.query.filter_by(userID=userID).first() 
    
    #Probably add some password hashing
    if (user is not None and user.key==password):  
        
        session['userID']=user.userID
        session['name']=user.name
        session['role']=user.role
     
        return redirect(url_for('login_get'))
    else:
        return render_template('login.html')


@app.route('/',methods = ['GET'])
@login_validation(role='admin')
def login_get(current_user):
    return render_template('index.html')

@app.route('/users/all',methods = ['GET'])
@login_validation(role='admin')
def web_users_all(current_user):
    all_users=users_all()
    return render_template('users.html',all_users=all_users)

@app.route('/contacts/all',methods = ['GET'])
@login_validation(role='admin')
def web_contacts_all(current_user):
    all_contacts=contacts_all()
    return render_template('contacts.html',all_contacts=all_contacts)


@app.route('/logout',methods = ['POST', 'GET'])
def logout():
    session.clear()
    return render_template('login.html')


"""
API Routes section
"""
    
#This should only be available to USERS with role=admin
@app.route('/api/contacts/all', methods=['GET'])
@token_validation(role='admin')
def api_contacts_all(current_user):
    all_contacts = contacts_all()
    return jsonify(all_contacts)

#This should only be available to USERS with role=user
@token_validation(role='users')
@app.route('/api/contacts/add', methods=['POST'])
def api_contacts_add():

    content = request.get_json()

    id_from = content['ID_FROM']
    id_to = content['ID_TO']
    dtg = content['DTG']

    conn = sqlite3.connect(DATABASE_URL)
    sql = ''' INSERT INTO CONTACTS(ID_FROM,ID_TO,DTG)
              VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql,(id_from, id_to,dtg))
    conn.commit()
    conn.close()
    
    return 'JSON Posted'

#This should only be available to USERS with role=admin
@token_validation(role='admin')
@app.route('/api/users/all', methods=['GET'])
def api_users_all():
    all_users = users_all()
    return jsonify(all_users)

#This should be un-authenticated, used for registering users
@app.route('/api/users/add', methods=['POST'])
def api_users_add():
    content = request.get_json()

    userID = content['userID']
    key = content['key']
    name = content['name']
    email = content['email']
    phone = content['phone']
   
    conn = sqlite3.connect(DATABASE_URL)
    sql = ''' INSERT INTO USERS(userID,NAME,KEY,EMAIL,PHONE)
              VALUES(?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql,(userID,name,key,email,phone))
    conn.commit()
    conn.close()
    return 'JSON Posted'

#This should be un-authenticated, used for registering users to obtain a token 
@app.route('/api/users/login', methods=['POST'])
def api_user_login():
    #Returns a JWT to an authorised user
    auth = request.get_json()   

    if not auth:  
        return make_response('could not verify', 401, {'WWW.Authentication': 'Basic realm: "login required"'})   
 
    user = Users.query.filter_by(userID=auth['userID']).first() 
    
    #Probably add some password hashing
    if user.key==auth['key']:  
        token = jwt.encode({'userID': user.userID, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])  
        return jsonify({'token' : token}) 

    return make_response('could not verify',  401, {'WWW.Authentication': 'Basic realm: "login required"'})

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
