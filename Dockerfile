FROM python:3.6-buster
WORKDIR /
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY app app/

WORKDIR /app
RUN python3 initdb.py
ENV FLASK_APP=app.py
#CMD flask run
CMD ["python", "app.py"]
