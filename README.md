# Contact Tracing App Prototype
This app utilises a flask API within a docker container to prototype a contact tracing app

## Installation
To install:
Provided you have docker installed, go to the directory with this README.md in it and run the following:

```
docker build -t tracer .
```
(tracer can be anything - it is the name of the container image)

## Start 
To run the app (default is debugging mode):

```
docker run -p 5000:5000 --name tracer tracer
```

To run the app utilising a specific app directory (outside of the container)

```
docker run -v /opt/Tracer/app:/app -p 5000:5000 --name tracer tracer
```
where /opt/Tracer/app is the directory external to the container. This is useful in development to reflect code changes within the container without having to rebuild the docker mage. Can also customise start.sh to suite your needs. 

## Database init
The database is initialized by initdb.py which is called when the docker image is built. This makes the tracer.db in the /app folder. When debugging in some IDE (eg VS Code) the database is expected in the folder one level up - you will need to move manually. Querying the database when a database does not exist will create a non-functional database file and result in error. 

## Database test data
The MOCK_CONTACTS and MOCK_USERS csv files are loaded into the main app database as part of initdby.py. The client side initdb.py contains one record for testing a contact upload. 

## Client Side
Client script is in the client directory and requires several libraries, be sure to install them (haven't had time to make requirements.txt). Currently the client generates a UUID and a key (another UUID) which will act as the Username/Password for the server (at this stage there is no authentication - any post requests to the server to add contacts/users is accepted. 

## Testing 
The script test_post.py is a template to test API endpoints.  

## TODO
+ Perform input validation on user entered fields
+ Tidy up some functionality in the client app such as disabling/enabling buttons at appropriate times etc
+ Client app bluetooth detection functionality



